PRODUCT_PACKAGES := \
    IM \
    VoiceDialer


$(call inherit-product, build/target/product/generic_with_google.mk)

# Overrides
PRODUCT_NAME := qcom_surf
PRODUCT_DEVICE := surf
