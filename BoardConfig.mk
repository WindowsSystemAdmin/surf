# config.mk
# 
# Product-specific compile-time definitions.
#

# BOARD_GPS_LIBRARIES := libgps

BOARD_CAMERA_LIBRARIES := libcamera

BOARD_OPENCORE_LIBRARIES := libOmxCore
BOARD_OPENCORE_FLAGS := -DHARDWARE_OMX=1

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true
TARGET_NO_RADIOIMAGE := true

TARGET_BOOTLOADER_LIBS := \
	libboot_board_surf \
	libboot_arch_msm7k \
	libboot_arch_armv6

TARGET_BOOTLOADER_LINK_SCRIPT := \
	vendor/qcom/surf/boot/boot.ld

BOARD_KERNEL_CMDLINE := mem=110M console=ttyMSM0,115200n8
